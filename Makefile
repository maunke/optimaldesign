PROJECT_DIR = ./optimaldesign

###################
### DEVELOPMENT ###
###################

install:  ## Install dependencies
	poetry install --no-root

update:  ## Update dependencies and export to requirements.txt
	poetry self update
	poetry update
	poetry export --without-hashes -f requirements.txt -o requirements.txt
	poetry export --without-hashes -f requirements.txt -o requirements_dev.txt --dev

lint: ## Lint code
	poetry run pflake8 $(PROJECT_DIR) --output-file flake8.txt || (flake8_junit flake8.txt junit.xml; exit 1)

type-check:
	poetry run mypy $(PROJECT_DIR) --junit-xml junit.xml

codequality:
	poetry run pflake8 --format gl-codeclimate --output-file codequality.json $(PROJECT_DIR)

pytest: 
	poetry run pytest $(PROJECT_DIR) --cov app --junitxml=junit.xml --cov-report xml:coverage.xml --cov-report term -vv

run-examples:
	poetry run jupyter notebook --notebook-dir=examples


help: ## show help message
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[$$()% 0-9a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

.PHONY: $(MAKECMDGOALS)
