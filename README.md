# Optimal Design

> This project is heavy under construction and is waiting for the v1.0 release.

This project implements efficiently the method "Adaptive grid Semidefinite Programming for finding optimal designs" (see literature) built on [JAX](https://jax.readthedocs.io/en/latest/). Therefore you can find optimal designs of arbitrary linear models on GPU, TPU or CPU.

## Example Colab Notebooks

- [2D Design Space](https://colab.research.google.com/drive/1udkw0mp-VONuoYRL7bl9gim3-Dk4KjmE?usp=sharing)
- [3D Design Space](https://colab.research.google.com/drive/1YlYfdYuHFLkWJ61IzP__R6N8koh_5Tzm?usp=sharing)

## Literature

- [Master Thesis - Markus Unkel (German)](literature/master-thesis-markusunkel-de.pdf)
